#ifndef LOWPASS_H
#define LOWPASS_H

#include <tuple>

#include <DspFilters/Dsp.h>

#include "floatingaveragecalculator.h"

template<class _FilterType = Dsp::Butterworth::LowPass<8>>
class Lowpass
{
public:
    typedef _FilterType FilterType;

    Lowpass(float cutoffFrequency = 2000, uint sampleRate = 48000) :
        _filterOrder(3), _sampleRate(sampleRate), _cutoffFrequency(cutoffFrequency)
    {
        setupFilter();
    }

    int filterOrder() const { return _filterOrder; }
    uint sampleRate() const { return _sampleRate; }
    float cutoffFrequency() const { return _cutoffFrequency; }
    float timeConstant() const { return 1.0 / (2.0 * M_PI * _cutoffFrequency); }

    void setFilterOrder(int arg) {
        if (_filterOrder == arg)
            return;

        _filterOrder = arg;
        setupFilter();
    }

    void setSampleRate(uint arg) {
        if (_sampleRate == arg)
            return;

        _sampleRate = arg;
        setupFilter();
    }

    void setTimeConstant(float arg) {
        setCutoffFrequency(1.0 / (2.0 * M_PI * arg));
    }

    void setCutoffFrequency(float arg) {
        if (_cutoffFrequency == arg)
            return;

        _cutoffFrequency = arg;
        setupFilter();
    }

    void operator()(float &a, float &b) {
        a = _states[0].process(a, _filter);
        b = _states[1].process(b, _filter);
    }

    const FilterType &filter() { return _filter; }

private:
    int _filterOrder;
    uint _sampleRate;
    float _cutoffFrequency;
    FilterType _filter;
    typename FilterType::template State<Dsp::DirectFormI<float, false>> _states[2];

    void setupFilter()
    {
        _filter.setup(_filterOrder, _sampleRate, _cutoffFrequency);
    }
};

#endif // LOWPASS

