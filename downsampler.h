#ifndef DOWNSAMPLER_H
#define DOWNSAMPLER_H

#include <cmath>
#include <tuple>

class Downsampler
{
public:
    Downsampler(unsigned int sourceRate = 48000, float targetRate = 20)
        : _sourceRate(sourceRate), _targetRate(targetRate), _nSamps(0)
    {
        _conversionFactor = std::round(static_cast<float>(sourceRate) / static_cast<float>(targetRate));
        reset();
    }

    void setSourceRate(unsigned int rate) {
        _sourceRate = rate;
        _conversionFactor = std::round(static_cast<float>(_sourceRate) / static_cast<float>(_targetRate));
        reset();
    }

    unsigned int sourceRate() const { return _sourceRate; }

    void setTargetRate(float rate) {
        _targetRate = rate;
        _conversionFactor = std::round(static_cast<float>(_sourceRate) / static_cast<float>(_targetRate));
        reset();
    }
    float targetRate() const { return _targetRate; }

    // resets the Downsampler so that the next sample will be kept.
    void reset() {
        _nSamps = _conversionFactor - 1;
        keepSample = false;
    }

    float actualTargetRate() const { return _sourceRate / static_cast<float>(_conversionFactor); }
    unsigned int conversionFactor() const { return _conversionFactor; }

    float timeSinceLastSample() const {
        return static_cast<float>(_nSamps) / _sourceRate;
    }

    bool operator()()
    {
        ++_nSamps;
        keepSample = (_nSamps - _conversionFactor) == 0;
        if (keepSample) {
            _nSamps = 0;
        }
        return keepSample;
    }

    bool keepSample;

private:
    unsigned int _sourceRate;
    float _targetRate;
    unsigned int _nSamps, _conversionFactor;
};

#endif // DOWNSAMPLER_H
