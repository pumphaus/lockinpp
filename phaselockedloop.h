#ifndef PHASELOCKEDLOOP_H
#define PHASELOCKEDLOOP_H

#include <tuple>

#include "floatingaveragecalculator.h"
#include "sinetable.h"

#include <iostream>
#include <cmath>
#include <complex>

class PhaseLockedLoop
{
public:
    PhaseLockedLoop() : periodInSamps(1), sampleCounter(0), freq(0.1), pllPhase(0) {
        periodAvg.resize(1500);
    }

    float currentFrequency(unsigned int sampleRate) const { return freq * sampleRate; }

    void setPhase(float phase) { pllPhase = phase / (2.0f * pi); }
    float phase() const { return pllPhase * (2.0f * pi); }

    inline void operator()(const float ref, float &x, float &y) {
        static const auto &sin = SineTable<float, 1024>::instance();

        ++sampleCounter;

        // we have the beginning of a rising edge! time to update our frequency
        bool curIsRising = ref > 0.5;

        if (curIsRising && !prevWasRising) {
            freq = periodAvg(periodInSamps).inv();

            if (std::abs(sin.atRelativePosition(freq * sampleCounter)) < 1e-2) {
                sampleCounter = 1;
            }

            periodInSamps = 0;
        }
        ++periodInSamps;
        prevWasRising = curIsRising;

        const float pos = freq * sampleCounter + pllPhase;

        x = sin.atRelativePosition(pos);
        y = sin.atRelativePosition(pos + 0.25);
    }

private:
    static constexpr float pi = M_PI;

    FloatingAverageCalculator<unsigned int, float> periodAvg;
    unsigned int periodInSamps, sampleCounter;
    float freq, pllPhase;
    bool prevWasRising;
};

#endif // PHASELOCKEDLOOP_H
