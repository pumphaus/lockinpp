#ifndef SINETABLE_H
#define SINETABLE_H

#include <type_traits>
#include <cstdlib>
#include <cmath>
#include <limits>

template<typename SampleType, size_t Size, typename = SampleType>
struct SineTable;

template<typename SampleType, size_t Size>
struct SineTable<SampleType, Size, typename std::enable_if<std::is_floating_point<SampleType>::value, SampleType>::type>
{
    SineTable() {
        for (size_t i = 0; i < Size; ++i) {
            this->table[i] = std::sin((2.0 * M_PI * i) / Size);
        }
    }

    SampleType operator()(SampleType pos) const {
        pos *= p2i;
        const size_t ipos = static_cast<size_t>(pos);
        const size_t id1 = ipos % Size;
        const size_t id2 = (id1 + 1) % Size;
        return lerp(table[id1], table[id2], pos - ipos);
    }

    SampleType atRelativePosition(const SampleType pos) const {
        const size_t ipos = fastRound(pos * Size) % Size;
        return table[ipos];
    }

    SampleType operator[](size_t i) const {
        return table[i];
    }

    static const SineTable& instance() {
        static SineTable st;
        return st;
    }

private:
    constexpr static size_t fastRound(const SampleType x) {
        return (size_t)(x + 0.5);
    }
    static SampleType lerp(SampleType v0, SampleType v1, SampleType t) {
        return (1-t)*v0 + t*v1;
    }

    SampleType table[Size];
    static constexpr SampleType p2i = Size / (2.0 * M_PI);
};

template<typename SampleType, size_t Size>
struct SineTable<SampleType, Size, typename std::enable_if<std::is_integral<SampleType>::value, SampleType>::type>
{
    SineTable() {
        for (size_t i = 0; i < Size; ++i) {
            this->table[i] = std::sin((2.0 * M_PI * i) / Size) * std::numeric_limits<SampleType>::max();
        }
    }

    SampleType operator()(float pos) const {
        const size_t ipos = static_cast<size_t>(std::round(pos * p2i)) % Size;
        return table[ipos];
    }

    SampleType operator[](size_t i) const {
        return table[i];
    }

    static const SineTable& instance() {
        static SineTable st;
        return st;
    }

private:
    SampleType table[Size];
    static constexpr float p2i = Size / (2.0 * M_PI);
};

#endif // SINETABLE_H
