#include "lockin.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

struct SwapAndAmplify {
    inline bool operator()(float &ref, float &sig) {
        std::swap(ref, sig);
        sig *= 200;
        return true;
    }
};

int main(int argc, char **argv)
{
    LockIn::LockIn<SwapAndAmplify> lia;

    std::ifstream data("/home/pumphaus/src/qtds-lockin/thz_r=48k_f=FLOAT32_chop=9471.raw", std::fstream::binary);

    std::cerr << "Reading data..." << std::endl;

    data.seekg(0, data.end);
    int length = data.tellg();
    data.seekg(0, data.beg);
    std::vector<float> frames(length / 4);
    data.read(reinterpret_cast<char*>(frames.data()), length);
    std::cerr << "Done reading " << length << " bytes. Processing..." << std::endl;

    lia.setSampleRate(48000);
    lia.setTimeConstant(0.08);
    lia.setIntermediaryFilterOrder(2);
    lia.setIntermediaryRate(2400);
    lia.setPhase(M_PI/4);

//     for (int i = 0; i < 20; ++i) {
        for (auto pair : lia(frames)) {
            std::fwrite(&pair.first, sizeof(float), 1, stdout);
            std::fwrite(&pair.second, sizeof(float), 1, stdout);
        }
//     }

    std::fflush(stdout);

    std::cerr << "Done with ref freq " << lia.referenceFrequency() << std::endl;
}
