#ifndef FLOATINGAVERAGECALCULATOR_H
#define FLOATINGAVERAGECALCULATOR_H

#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#include <boost/circular_buffer.hpp>
#pragma GCC diagnostic pop

template<typename T, typename U>
struct lazy_fraction {
    T x; U y;
    lazy_fraction(const T &x, const U &y) : x(x), y(y) {}
    operator decltype(x / y)() { return x / y; }
    auto eval() const -> decltype(x / y) { return x / y; }
    lazy_fraction<U, T> inv() const { return lazy_fraction<U, T>(y, x); }

    template<typename M, typename N>
    auto operator *(const lazy_fraction<M, N> &other) const
        -> lazy_fraction<decltype(x * other.x), decltype(y * other.y)>
    {
        return lazy_fraction<decltype(x * other.x), decltype(y * other.y)>(x * other.x, y * other.y);
    }

    template<typename M, typename N>
    auto operator /(const lazy_fraction<M, N> &other) const
        -> lazy_fraction<decltype(x * other.y), decltype(y * other.x)>
    {
        return lazy_fraction<decltype(x * other.y), decltype(y * other.x)>(x * other.y, y * other.x);
    }
};

template<typename T, typename U = double>
class FloatingAverageCalculator
{
public:
    FloatingAverageCalculator(size_t n = 0, const T &init = T())
        : buf(n), acc(init), init(init)
    {
    }

    void initialize(size_t n, const T &init = T())
    {
        this->init = init;
        resize(n);
        clear();
    }

    lazy_fraction<T, U> avg() const
    {
        return lazy_fraction<T, U>(acc, fill());
    }

    T accumulated() const {
        return acc;
    }

    void addData(const T &v)
    {
        if (buf.full())
            acc -= buf.back();

        acc += v;

        buf.push_front(v);
    }

    void clear()
    {
        buf.clear();
        acc = init;
    }

    void resize(size_t n)
    {
        buf.set_capacity(n);
    }

    lazy_fraction<T, U> operator()() {
        return avg();
    }

    lazy_fraction<T, U> operator()(const T& v) {
        addData(v);
        return avg();
    }

    size_t size() const { return buf.capacity(); }
    size_t fill() const { return buf.size(); }
    bool isFull() const { return buf.full(); }

private:
    boost::circular_buffer<T> buf;
    T acc, init;
};

#define _LAZY_FRACTION_OPERATORS(op, x_by_frac, frac_by_x) \
    template<typename X, typename T, typename U> \
    auto operator op(const X &x, const lazy_fraction<T, U> &frac) -> decltype(x_by_frac) \
    { \
        return x_by_frac; \
    } \
    template<typename T, typename U, typename X> \
    auto operator op(const lazy_fraction<T, U> &frac, const X &x) -> decltype(frac_by_x) \
    { \
        return frac_by_x; \
    } \

// multiplication and division, yielding best accuracy and performance for integer operations
_LAZY_FRACTION_OPERATORS(/, (x * frac.y) / frac.x, frac.x / (frac.y * x))
_LAZY_FRACTION_OPERATORS(*, (x * frac.x) / frac.y, (x * frac.x) / frac.y)

// relational operators, all following the same pattern
_LAZY_FRACTION_OPERATORS(>, (x * frac.y) > frac.x, frac.x > (x * frac.y))
_LAZY_FRACTION_OPERATORS(>=, (x * frac.y) >= frac.x, frac.x >= (x * frac.y))
_LAZY_FRACTION_OPERATORS(<, (x * frac.y) < frac.x, frac.x < (x * frac.y))
_LAZY_FRACTION_OPERATORS(<=, (x * frac.y) <= frac.x, frac.x <= (x * frac.y))
_LAZY_FRACTION_OPERATORS(==, (x * frac.y) == frac.x, frac.x == (x * frac.y))
_LAZY_FRACTION_OPERATORS(!=, (x * frac.y) != frac.x, frac.x != (x * frac.y))

#undef _LAZY_FRACTION_OPERATORS

#endif // FLOATINGAVERAGECALCULATOR_H
