#ifndef LOCKIN_H
#define LOCKIN_H

#include <utility>

#include "downsampler.h"
#include "lowpass.h"
#include "phaselockedloop.h"

namespace LockIn {

struct Nop
{
    constexpr void operator()(float, float) {}
};

template<class PreProcessorType = Nop,
         class PostProcessorType = Nop,
         class FilterType = Dsp::Butterworth::LowPass<8>>
class LockIn
{
public:
    LockIn() : validOutputFrame(false) {
        setIntermediaryFilterOrder(2);
        setFilterOrder(4);
        setIntermediaryRate(2400);
        setSampleRate(48000);
        setTimeConstant(0.08);
    }

    unsigned int intermediaryRate() const { return smooth2.sampleRate(); }
    void setIntermediaryRate(unsigned int rate) {
        smooth1.setCutoffFrequency(rate / 4);
        downsample1.setTargetRate(rate);
        smooth2.setSampleRate(rate);
        downsample2.setSourceRate(rate);
    }
    void setIntermediaryFilterOrder(unsigned int order) { smooth1.setFilterOrder(order); }
    unsigned int intermediaryFilterOrder() const { return smooth1.filterOrder(); }

    void setFilterOrder(unsigned int order) { smooth2.setFilterOrder(order); }
    unsigned int filterOrder() const { return smooth2.filterOrder(); }

    unsigned int sampleRate() const { return smooth1.sampleRate(); }
    void setSampleRate(unsigned int sampleRate) {
        smooth1.setSampleRate(sampleRate);
        downsample1.setSourceRate(sampleRate);
    }

    float outputRate() const { return downsample2.actualTargetRate(); }
    void setOutputRate(unsigned int outputRate) { downsample2.setTargetRate(outputRate); }

    float timeConstant() const { return smooth2.timeConstant(); }
    void setTimeConstant(float tau) { smooth2.setTimeConstant(tau); }

    float cutoffFrequency() const { return smooth2.cutoffFrequency(); }
    void setCutoffFrequency(float cutoffFrequency) { smooth2.setCutoffFrequency(cutoffFrequency); }

    void setPhase(float phase) { pll.setPhase(phase); }
    float phase() const { return pll.phase(); }

    float referenceFrequency() const { return pll.currentFrequency(sampleRate()); }

    inline bool processFrame(float ref, float sig) {
        preprocess(ref, sig);

        pll(ref, xy.first, xy.second);

        xy.first *= sig;
        xy.second *= sig;

        smooth1(xy.first, xy.second);

        if (!(validOutputFrame = downsample1())) {
            return false;
        }

        smooth2(xy.first, xy.second);

        if (!(validOutputFrame = downsample2())) {
            return false;
        }

        postprocess(xy.first, xy.second);

        return true;
    }

    struct iterator;

    inline iterator begin(const float *frames, size_t n) {
        return iterator(this, frames, n);
    }

    inline const iterator &end() const {
        static iterator endIt(nullptr, 0, 0);
        return endIt;
    }

    struct ReturnIteratorWrapper {
        explicit ReturnIteratorWrapper(LockIn *lockin, const float *frames, const size_t n)
            : lockIn(lockin), frames(frames), n_frames(n) {}

        LockIn::iterator begin() { return lockIn->begin(frames, n_frames); }
        const LockIn::iterator &end() const { return lockIn->end(); }

    private:
        LockIn *lockIn;
        const float *frames;
        const size_t n_frames;
    };

    ReturnIteratorWrapper operator()(const float *frames, const size_t n) {
        return ReturnIteratorWrapper(this, frames, n);
    }

    ReturnIteratorWrapper operator()(const std::vector<float> &frames) {
        return ReturnIteratorWrapper(this, frames.data(), frames.size() / 2);
    }

    // n: actual count of _frames_, not samples
    template<class CallbackType>
    inline void processFrames(const float *frames, size_t n, CallbackType callback) {
        for (size_t i = 0; i < n; ++i) {
            if (processFrame(frames[2 * i], frames[2 * i + 1])) {
                callback(xy.first, xy.second);
            }
        }
    }

    std::pair<float, float> xy;
    bool validOutputFrame;

    Lowpass<FilterType> smooth1, smooth2;
    Downsampler downsample1, downsample2;
    PhaseLockedLoop pll;

    PreProcessorType preprocess;
    PostProcessorType postprocess;
};

template<class FilterType, class PreProcessorType, class PostProcessorType>
struct LockIn<FilterType, PreProcessorType, PostProcessorType>::iterator {
    explicit iterator(LockIn *lockIn, const float *frames, size_t n)
        : m_lockIn(lockIn), m_frames(frames), m_n(n), m_i(0)
    {
    }
    bool pastEnd() const { return m_i >= m_n; }

    iterator &operator++() {
        for (; !pastEnd(); ++m_i) {
            m_lockIn->processFrame(m_frames[2 * m_i], m_frames[2 * m_i + 1]);
            if (m_lockIn->validOutputFrame) {
                xy = m_lockIn->xy;
                break;
            }
        }
        return *this;
    }

    iterator operator++(int) {
        iterator copy = *this;
        ++*this;
        return copy;
    }

    bool operator==(const iterator &other) const {
        return pastEnd() == other.pastEnd();
    }

    bool operator!=(const iterator &other) const {
        return pastEnd() != other.pastEnd();
    }

    const std::pair<float, float> &operator*() const {
        return xy;
    }

    const std::pair<float, float> *operator->() const {
        return &xy;
    }

private:
    std::pair<float, float> xy;
    LockIn *m_lockIn;
    const float *m_frames;
    size_t m_n, m_i;
};

}

#endif // LOCKIN_H
